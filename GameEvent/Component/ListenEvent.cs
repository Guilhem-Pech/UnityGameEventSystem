using System;
using System.Collections.Generic;
using GameEvent;
using UnityEngine;
using Event = GameEvent.Event;

public class ListenEvent : MonoBehaviour
{
    private Stack<IEvent> _eventStack = new Stack<IEvent>();
    private Dictionary<string, List<Action<IEvent>>> _callbacks = new Dictionary<string, List<Action<IEvent>>>();
    private void Update()
    {
        while (_eventStack.Count > 0)
        {
            IEvent e = _eventStack.Pop();
            if (_callbacks.TryGetValue(e.GetName(), out List<Action<IEvent>> value))
            {
                value.ForEach(c => c.Invoke(e));
            }
        }
    }

    public void SendEvent(IEvent anEvent)
    {
        if (!_callbacks.TryGetValue(anEvent.GetName(), out List<Action<IEvent>> value)) return;
        foreach (Action<IEvent> action in value)
        {
            action?.Invoke(anEvent);
        }
    }
    
    public void SendAsyncEvent(IEvent anEvent)
    {
        _eventStack.Push(anEvent);
    }

    public void ListenToEvent<T> (Action<IEvent> method) where T : IEvent
    {
        var key = typeof(T).FullName ?? throw new InvalidOperationException();
        if (_callbacks.ContainsKey(key))
        {
            _callbacks[key].Add(method);
        }
        else
        {
            _callbacks.Add(key,new List<Action<IEvent>> {method});
        }
    }


    public void UnregisterEvent<T>(Action<IEvent> method)
    {
        _callbacks.Remove(typeof(T).FullName ?? throw new InvalidOperationException());
    }
}
