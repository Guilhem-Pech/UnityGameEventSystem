﻿using UnityEngine;
using UnityEngine.Events;

namespace GameEvent
{
    public interface IEvent
    {
        string GetName();
    }

    public class Event : IEvent
    {
        private readonly string name;


        protected Event()
        {
            this.name = this.GetType().FullName; 
        }

        public string GetName()
        {
            return name;
        }
    }
}