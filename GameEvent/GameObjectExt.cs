﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameEvent
{
    public static class GameObjectExt
    {

        /// <summary>
        /// Function that get the first object with the specific component, create an empty gameobject withe the component otherwise
        /// CAUTION: This function is heavy and should not be used every frame (you should cache the result)
        /// </summary>
        /// <param name="obj"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetDefault<T>(this Object obj) where T : MonoBehaviour
        {
            T value = Object.FindObjectOfType<T>();
            if (value != null)
            {
                return value;
            }
            GameObject go = new GameObject(typeof(T).FullName);
            return go.AddComponent<T>();
        }
        
        
        /// <summary>
        /// Get an event Listener of a gameobject, or create it if none
        /// </summary>
        /// <param name="go"></param>
        /// <returns></returns>
        public static ListenEvent GetEventListener(this GameObject go)
        {
            if (go.TryGetComponent(out ListenEvent listenEvent))
            {
                return listenEvent;
            }
            listenEvent = go.AddComponent<ListenEvent>();
            return listenEvent;
        }
        
        /// <summary>
        /// Listen to an event on a gameobject
        /// Caution, this method is expensive as it check if a component has a ListenEvent component beforehand,
        /// please consider caching it. 
        /// </summary>
        /// <param name="go"></param>
        /// <param name="method"></param>
        /// <typeparam name="T"></typeparam>
        public static void ListenToEvent<T>(this GameObject go, Action<IEvent> method) where T : IEvent
        {
            if (go.TryGetComponent(out ListenEvent listenEvent))
            {
                listenEvent.ListenToEvent<T>(method);
            }
            else
            {
                listenEvent = go.AddComponent<ListenEvent>();
                listenEvent.ListenToEvent<T>(method);
            }
            
        }
        
        /// <summary>
        /// Send an event on a gameobject
        /// Caution, this method is expensive as it check if a component has a ListenEvent component beforehand,
        /// please consider caching it.
        /// </summary>
        /// <param name="go"></param>
        /// <param name="anEvent"></param>
        /// <typeparam name="T"></typeparam>
        public static void SendEvent<T>(this GameObject go, T anEvent) where T : Event
        {
            if (go.TryGetComponent(out ListenEvent listenEvent))
            {
                listenEvent.SendEvent(anEvent);
            }
            else
            {
                listenEvent = go.AddComponent<ListenEvent>();
                listenEvent.SendEvent(anEvent);
            }
        }
        
    }
}