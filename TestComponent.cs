using System;
using System.Collections;
using System.Collections.Generic;
using GameEvent;
using UnityEngine;
using Event = GameEvent.Event;

public class TestComponent : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private ListenEvent _listenEvent;
    void Start()
    {
        _listenEvent.ListenToEvent<MyCustomEvent>(OnReceiveFirstEvent);
        _listenEvent.ListenToEvent<MySecondEvent>(OnReceiveSecondEvent);
        
    }

    private void OnReceiveSecondEvent(IEvent obj)
    {
        MySecondEvent val = obj as MySecondEvent;
        print(val.foo);
    }

    private void OnReceiveFirstEvent(IEvent obj)
    {
        MyCustomEvent val = obj as MyCustomEvent;
        print(val.stuff);
    }


    private void OnEnable()
    {
        MyCustomEvent myEvent = new MyCustomEvent{stuff = 123};

        MySecondEvent my2ndEvent = new MySecondEvent {foo = 0.132f};

        _listenEvent.SendEvent(myEvent);
        _listenEvent.SendEvent(my2ndEvent);
        _listenEvent.SendEvent(new MyThirdEvent() {fee = "LeL"});

    }
}

public class MyCustomEvent : Event
{
    public int stuff;
}

public class MySecondEvent : Event
{
    public float foo;
}

public class MyThirdEvent : Event
{
    public string fee;
}